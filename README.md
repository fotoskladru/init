#Yii2 aplication init, deploy and configure

##Usage


Init new project
----------------

```bash
sudo -u postgres createuser -P USERNAME
sudo -u postgres createdb -O USERNAME DBNAME
cd {PROJECT_DIR}
composer require "fotoskladru/init:~1.0"
./vendor/bin/init create --env={ENVIRONMENT}
```

Edit config file `{PROJECT_DIR}/deploy/config.php`

Edit variables file `{PROJECT_DIR}/runtime/vars/map-{ENVIRONMENT}.php`

```bash
./init config --env={ENVIRONMENT}
./init main --env={ENVIRONMENT} --nginx=1 --custom=1
```


Deploy existing project
----------------

```bash
sudo -u postgres createuser -P USERNAME
sudo -u postgres createdb -O USERNAME DBNAME
git clone {PROJECT_REPO} {PROJECT_DIR}
cd {PROJECT_DIR}
./init map --env={ENVIRONMENT}
```

Edit variables file `{PROJECT_DIR}/runtime/vars/map-{ENVIRONMENT}.php`

```bash
./init config --env={ENVIRONMENT}
./init main --env={ENVIRONMENT} --nginx=1 --custom=1
```


Update config
----------------

```bash
./init config --env={ENVIRONMENT}
```

If not assigned values, edit variables file `{PROJECT_DIR}/runtime/vars/map-{ENVIRONMENT}.php` and try `init config` again:


Update remote config
----------------

```bash
./init config --env={REMOTE_ENVIRONMENT} --remote={REMOTE_NAME}
```

If not assigned values, edit variables file `{PROJECT_DIR}/runtime/vars/map-{REMOTE_ENVIRONMENT}.php` and try `init config` again:

If you need priority of remote variable values, use:

```bash
./init config --env={REMOTE_ENVIRONMENT} --remote={REMOTE_NAME} --priority=remote
```


Run custom code in deploy/config.php (migrations, fixtures, images etc.)
----------------
```bash
./init custom --env={ENVIRONMENT}
```


Create nginx configs in /etc/nginx and update /etc/hosts
----------------
```bash
./init nginx --env={ENVIRONMENT}
```