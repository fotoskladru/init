<?php

namespace fs\init;

use primipilus\guid\GuidHelper;

/**
 * Class Config
 *
 * @package fs\init
 */
class Config
{

    const PRIORITIES = ['local', 'remote'];
    const MAP_SUBDIR = 'runtime' . DIRECTORY_SEPARATOR . 'vars';

    /**
     * @var Env
     */
    protected $_env;

    /**
     * @var string
     */
    protected $_branch;

    /**
     * @var string
     */
    protected $_fromDir;

    /**
     * @var array
     */
    protected $_files = [];

    /**
     * @var array
     */
    protected $_varNames = [];

    /**
     * @var string
     */
    protected $_mapDir;

    /**
     * @var string
     */
    protected $_mapPath;

    /**
     * @var string
     */
    protected $_remoteMapPath;

    /**
     * @var string
     */
    protected $_localCopyMapPath;

    /**
     * @var string
     */
    protected $_priority;

    /**
     * @var array
     */
    protected $_vars = [];

    /**
     * Config constructor.
     *
     * @param Env $env
     *
     * @throws \ErrorException
     */
    public function __construct(Env $env)
    {
        $this->_env = $env;
        $envName = $env->getName();
        $envFullName = $env->getFullName();
        $envBranch = $env->getEnvBranch();
        $remoteName = $env->getParam('remote');

        if ($remoteName !== null && $env->getCurrentBranch() != $envBranch) {
            throw new \ErrorException("Before running, you must switch your repository to branch '{$envBranch}' and commit your changes");
        }

        $this->_fromDir = $env->getProjectRoot() . DIRECTORY_SEPARATOR . 'environments' . DIRECTORY_SEPARATOR . $envFullName;

        $dirIterator = new \RecursiveDirectoryIterator($this->_fromDir, \FilesystemIterator::KEY_AS_PATHNAME | \FilesystemIterator::CURRENT_AS_FILEINFO | \FilesystemIterator::SKIP_DOTS);
        $iterator = new \RecursiveIteratorIterator($dirIterator);
        $regexIterator = new \RegexIterator($iterator, '/^.+\-local\.php$/i', \RecursiveRegexIterator::GET_MATCH);

        foreach ($regexIterator as $item) {
            $this->_files[] = substr($item[0], strlen($this->_fromDir));
        }

        foreach ($this->_files as $file) {
            $this->_varNames = array_merge($this->_varNames, $this->extractVars($this->_fromDir . $file));
        }

        $duplicated = $this->getDuplicatedVars($this->_varNames);
        if ($duplicated) {
            throw new \ErrorException("Files contains duplicated variables: " . implode(', ', $duplicated) . '.');
        }

        $this->_mapDir = $env->getProjectRoot() . DIRECTORY_SEPARATOR . static::MAP_SUBDIR;
        $this->_mapPath = $this->_mapDir . DIRECTORY_SEPARATOR . "map-{$envName}.php";

        if ($remoteName !== null) {
            if (!isset($env->config['remotes'][$remoteName]) || !is_array($env->config['remotes'][$remoteName])) {
                throw new \ErrorException("Specified remote server '{$remoteName}' not found in the configuration");
            }
            $remote = $env->config['remotes'][$remoteName];
            $this->_remoteMapPath = $remote['project_root'] . DIRECTORY_SEPARATOR . static::MAP_SUBDIR . DIRECTORY_SEPARATOR . "map-{$envName}.php";
            $this->_localCopyMapPath = $this->_mapDir . DIRECTORY_SEPARATOR . "map-{$envName}-remote-{$remoteName}.php";
        }

        $this->_priority = $env->getParam('priority', Command::getDefault($env->getCommandName(), 'priority'));

        if (file_exists($this->_mapPath)) {
            $this->_vars = $this->loadVars($this->_mapPath);
        } elseif ($this->_varNames) {
            $this->_vars = $this->getVarsFromNames($this->_varNames);
        }
    }

    /**
     * Create variables map
     *
     * @throws \ErrorException
     */
    public function createMap() : void
    {
        if (file_exists($this->_mapPath)) { //если карта уже создана, обновляем её
            $notAvailableVarsNames = $this->checkForNotAvailableVars();
            if ($notAvailableVarsNames) {
                $notAvailableVars = $this->getVarsFromNames($notAvailableVarsNames);
                $this->_vars = $this->saveVars($this->_mapPath, $notAvailableVars);
            }
        } elseif ($this->_varNames) { //иначе, если в шаблонах есть переменные, создаем карту
            @mkdir($this->_mapDir, ENV::DIR_PERMISSIONS, true);
            $this->saveVars($this->_mapPath, $this->_vars);
        }

        $env = $this->_env;
        $remoteName = $env->getParam('remote');
        if ($remoteName !== null) { //если deploy на другую машину, то получаем удаленный map-файл и мержим его с локальным
            $remote = $env->config['remotes'][$remoteName];
            $scpPort = isset($remote['port']) ? "-P {$remote['port']}" : '';

            $env->exec("scp {$scpPort} {$remote['user']}@{$remote['domain']}:{$this->_remoteMapPath} {$this->_localCopyMapPath}", true);
            $remoteVars = file_exists($this->_localCopyMapPath) ? $this->loadVars($this->_localCopyMapPath) : [];
            if ($remoteVars) {
                if ($this->_vars) {
                    $existVarsNames = array_intersect_key($this->_vars, $remoteVars);
                    $newRemoteVarsNames = array_diff_key($remoteVars, $this->_vars);
                    foreach ($newRemoteVarsNames as $newRemoteVarName => $newRemoteVarValue) {
                        $this->_vars[$newRemoteVarName] = $remoteVars[$newRemoteVarName];
                    }
                    if ($this->_priority == 'remote') {
                        foreach ($existVarsNames as $existVarName => $existVarValue) {
                            $this->_vars[$existVarName] = $remoteVars[$existVarName];
                        }
                    }
                    unlink($this->_mapPath);
                    $this->saveVars($this->_mapPath, $this->_vars);
                } else {
                    $this->_vars = $this->saveVars($this->_mapPath, $remoteVars);
                }
            }
        }

        $emptyVarsNames = $this->checkForEmptyVars();
        if ($emptyVarsNames) {
            throw new \ErrorException("Not assigned values to variables in map-file: " . implode(', ', $emptyVarsNames) . '.');
        }
    }

    /**
     * Create configs based on variables map
     *
     * @throws \ErrorException
     */
    public function createConfig() : void
    {
        $env = $this->_env;
        $remoteName = $env->getParam('remote');
        if ($remoteName === null) { //на локальной машине
            foreach ($this->_files as $file) {
                $content = file_get_contents($this->_fromDir . $file);
                $names = $this->extractVars($this->_fromDir . $file);
                foreach ($names as $name) {
                    $regexpName = preg_quote($name);
                    $content = preg_replace("/({{\s*{$regexpName}\s*}})/", $this->_vars[$name], $content);
                }
                file_put_contents($env->getProjectRoot() . $file, $content);
            }
        } else { //на удаленной машине
            $remote = $env->config['remotes'][$remoteName];
            $scpPort = isset($remote['port']) ? "-P {$remote['port']}" : '';
            $sshPort = isset($remote['port']) ? "-p {$remote['port']}" : '';
            $envName = $env->getName();
            $envBranch = $env->getEnvBranch();
            $remoteCommands = ["cd {$remote['project_root']}"];

            if (!empty($env->config['remote_commands']['before']['common'])) {
                foreach ($env->config['remote_commands']['before']['common'] as $cmd) {
                    $remoteCommands[] = $cmd;
                }
            }
            if (!empty($env->config['remote_commands']['before'][$envName])) {
                foreach ($env->config['remote_commands']['before'][$envName] as $cmd) {
                    $remoteCommands[] = $cmd;
                }
            }
            $remoteCommands[] = "git pull origin {$envBranch}";
            $remoteCommands[] = "./init config --env={$envName}";

            if (!empty($env->config['remote_commands']['after']['common'])) {
                foreach ($env->config['remote_commands']['after']['common'] as $cmd) {
                    $remoteCommands[] = $cmd;
                }
            }
            if (!empty($env->config['remote_commands']['after'][$envName])) {
                foreach ($env->config['remote_commands']['after'][$envName] as $cmd) {
                    $remoteCommands[] = $cmd;
                }
            }

            $remoteCommands = implode(' && ', $remoteCommands);

            $env->exec("git push origin {$envBranch}", true);
            $env->exec("scp {$scpPort} {$this->_mapPath} {$remote['user']}@{$remote['domain']}:{$this->_remoteMapPath}", true);
            $env->exec("ssh {$remote['user']}@{$remote['domain']} {$sshPort} '{$remoteCommands}'", true);
        }
    }

    /**
     * Return list of variables names from templates
     *
     * @param string $file
     *
     * @return array
     *
     * @throws \ErrorException
     */
    protected function extractVars(string $file) : array
    {
        $content = file_get_contents($file);
        preg_match_all('/{{\s*([A-Za-z0-9_.:()]+)\s*}}/', $content, $matches);
        $varNames = $matches[1];

        $duplicated = $this->getDuplicatedVars($varNames);

        if ($duplicated) {
            throw new \ErrorException("File '{$file}' contains duplicated variables: " . implode(', ', $duplicated) . '.');
        }

        return $varNames;
    }

    /**
     * Returns an array with duplicate variables names
     *
     * @param array $data
     *
     * @return array
     */
    protected function getDuplicatedVars(array $data) : array
    {
        $duplicated = [];
        $uniqueNames = array_unique($data);
        foreach ($uniqueNames as $name) {
            $keys = array_keys($data, $name);
            if (count($keys) > 1) {
                $duplicated[] = $name;
            }
        }

        return $duplicated;
    }

    /**
     * Returns an array with variables and their default values
     *
     * @param array $names
     *
     * @return array
     */
    protected function getVarsFromNames(array $names) : array
    {
        $security = new \yii\base\Security();
        $vars = [];
        foreach ($names as $name) {
            if (false === strpos($name, ':')) {
                $vars[$name] = '';
            } else {
                [$varName, $default] = explode(':', $name);
                $default = trim($default);
                if ($default == 'guid') {
                    $vars[$name] = (string)GuidHelper::createGeneratedGuid();
                } elseif (preg_match('/^string\((\d+)\)$/', $default, $matches)) {
                    $vars[$name] = $security->generateRandomString((int)$matches[1]);
                } else {
                    $vars[$name] = '';
                }
            }
        }

        return $vars;
    }

    /**
     * Create/update variables map
     *
     * @param string $file
     * @param array $data
     *
     * @return array
     */
    protected function saveVars(string $file, array $data) : array
    {
        $vars = file_exists($file) ? $this->loadVars($file) : [];
        $data = array_replace($data, $vars);
        $content = var_export($data, true);
        $content = '<?php' . PHP_EOL . 'return ' . $content . ';';
        file_put_contents($file, $content);

        return $data;
    }

    /**
     * Load variables map
     *
     * @param string $file
     *
     * @return array
     */
    protected function loadVars(string $file) : array
    {
        return require $file;
    }

    /**
     * Returns an array with unavailable variables names
     *
     * @return array
     */
    protected function checkForNotAvailableVars() : array
    {
        $notAvailableVarsNames = [];
        foreach ($this->_varNames as $name) {
            if (!key_exists($name, $this->_vars)) {
                $notAvailableVarsNames[] = $name;
            }
        }

        return $notAvailableVarsNames;
    }

    /**
     * Returns an array with variables names with empty value
     *
     * @return array
     */
    protected function checkForEmptyVars() : array
    {
        $emptyVarsNames = [];
        foreach ($this->_varNames as $name) {
            if ($this->_vars[$name] === '') {
                $emptyVarsNames[] = $name;
            }
        }

        return $emptyVarsNames;
    }
}