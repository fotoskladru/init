<?php

namespace fs\init;

/**
 * Class Init
 *
 * @package fs\init
 */
class Init
{
    /**
     * @var Env
     */
    protected $_env;

    /**
     * Init constructor.
     *
     * @param Env $env
     *
     * @throws \ErrorException
     */
    public function __construct(Env $env)
    {
        $this->_env = $env;
    }

    /**
     * @throws \ErrorException
     */
    public function createSkeleton() : void
    {
        $env = $this->_env;
        $projectRoot = $env->getProjectRoot();

        if (!file_exists($projectRoot . DIRECTORY_SEPARATOR . '.git')) {
            $env->exec('git init');
            $env->exec('git remote add origin git@bitbucket.org:fotoskladru/skeleton.git');
            $env->exec('git fetch');
            $env->exec('git checkout -t origin/master');
            $env->exec('git pull origin master');
            $env->exec('rm -rf .git');
            $env->exec('git init');
        }

        $vendorPath = dirname(dirname(dirname(__DIR__)));
        $vendorDirName = str_replace($projectRoot . DIRECTORY_SEPARATOR, '', $vendorPath);
        $initPath = '.' . DIRECTORY_SEPARATOR . $vendorDirName . DIRECTORY_SEPARATOR . 'fotoskladru' . DIRECTORY_SEPARATOR . 'init' . DIRECTORY_SEPARATOR . 'bin' . DIRECTORY_SEPARATOR . 'init';
        $linkPath = $projectRoot . DIRECTORY_SEPARATOR . 'init';
        $env->exec("ln -s {$initPath} {$linkPath}");

        $conf = new Config($env);
        $conf->createMap();
    }

    /**
     * @throws \ErrorException
     */
    public function mainRun() : void
    {
        $env = $this->_env;
        $command = $env->getCommandName();

        if ($env->getBoolParam('perm', Command::getDefault($command, 'perm'))) {
            $this->directoryPermissions();
        }

        if ($env->getBoolParam('local', Command::getDefault($command, 'local'))) {
            $this->createLocal();
        }

        if ($env->getBoolParam('nginx', Command::getDefault($command, 'nginx'))) {
            $this->setHosts();
            $this->setNginx();
        }

        if ($env->getBoolParam('custom', Command::getDefault($command, 'custom'))) {
            $this->execCustomCode();
        }
    }

    public function directoryPermissions() : void
    {
        $env = $this->_env;
        if ($env->isStage() || $env->isProduction()) {
            foreach ([
                        'runtime',
                        'welcome' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'upload',
                        'welcome' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'assets',
                        'welcome' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'sitemap',
                        'welcome' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'robots',
                        'staff' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'assets'
                     ] as $dir) {
                echo 'chmod ' . $dir . ' ' . Env::DIR_PERMISSIONS, PHP_EOL;
                @umask(0);
                chmod($env->getProjectRoot() . DIRECTORY_SEPARATOR . $dir, Env::DIR_PERMISSIONS);
            }
        }
    }

    /**
     * @throws \ErrorException
     */
    public function createLocal() : void
    {
        $env = $this->_env;
        $projectRoot = $env->getProjectRoot();
        $envName = $env->getName();

        if (empty($env->config['webroots'][$envName]['welcome']) || empty($env->config['webroots'][$envName]['staff'])) {
            throw new \ErrorException("Not found webroots for '{$envName}' environment in the configuration");
        }
        $welcome = $env->config['webroots'][$envName]['welcome'];
        $staff = $env->config['webroots'][$envName]['staff'];

        $content = ['<?php'];
        if ($env->isDevelopment()) {
            $content[] = 'defined(\'__PROJECT_DIR__\') or define(\'__PROJECT_DIR__\', \'' . $projectRoot . '\');';
            $content[] = 'defined(\'YII_DEBUG\') or define(\'YII_DEBUG\', true);';
        } else {
            $content[] = 'defined(\'__PROJECT_DIR__\') or define(\'__PROJECT_DIR__\', __DIR__, true);';
        }
        $content[] = 'defined(\'YII_ENV\') or define(\'YII_ENV\', \'' . $envName . '\');';
        $content[] = '';
        $content[] = 'defined(\'WELCOME_WEB_ROOT\') or define(\'WELCOME_WEB_ROOT\', \''.$welcome.'\');';
        $content[] = 'defined(\'STAFF_WEB_ROOT\') or define(\'STAFF_WEB_ROOT\', \''.$staff.'\');';

        $welcomeParts = parse_url($welcome);
        $staffParts = parse_url($staff);

        $content[] = '';
        $content[] = 'defined(\'WELCOME_WEB_DOMAIN\') or define(\'WELCOME_WEB_DOMAIN\', \'' . $welcomeParts['host'] . '\');';
        $content[] = 'defined(\'STAFF_WEB_DOMAIN\') or define(\'STAFF_WEB_DOMAIN\', \'' . $staffParts['host'] . '\');';

        file_put_contents("{$projectRoot}/local.php", implode(PHP_EOL, $content));
    }

    public function setHosts() : void
    {
        $env = $this->_env;
        $envName = $env->getName();
        if ($env->isDevelopment()) {
            $env->exec('sudo cat /etc/hosts', false, $out);

            if (isset($env->config['hosts'][$envName])) {
                foreach ($env->config['hosts'][$envName] as $host) {
                    foreach ($out as $line) {
                        if (preg_match('#127\.0\.0\.1(.*)' . addcslashes($host, '.') . '#', $line)) {
                            continue 2;
                        }
                    }

                    $env->exec('sudo sh -c \'echo "127.0.0.1' . "\t" . $host . '" >> /etc/hosts\'');
                }
            }
        }
    }

    public function setNginx() : void
    {
        $env = $this->_env;
        $projectRoot = $env->getProjectRoot();
        $envName = $env->getName();
        $envFullName = $env->getFullName();
        $path = $projectRoot . DIRECTORY_SEPARATOR . 'environments' . DIRECTORY_SEPARATOR . $envFullName . DIRECTORY_SEPARATOR . 'nginx'. DIRECTORY_SEPARATOR . 'project.conf';
        $projectPath = explode(DIRECTORY_SEPARATOR, $projectRoot);
        $projectName = array_pop($projectPath) . ".{$envName}";

        $nginxConf = file_get_contents($path);
        $nginxConf = str_replace('{{webroot}}', $projectRoot, $nginxConf);
        if (!empty($env->config['hosts'][$envName])) {
            foreach ($env->config['hosts'][$envName] as $name => $host) {
                $nginxConf = str_replace("{{{$name}}}", $host, $nginxConf);
            }
        }
        $configFile = $projectRoot . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . $envFullName . DIRECTORY_SEPARATOR . 'nginx' . DIRECTORY_SEPARATOR . $projectName;

        // umask(0) is for correct recursive dir permissions
        $umask = umask();
        umask(0);
        @mkdir(dirname($configFile), Env::DIR_PERMISSIONS, true);
        file_put_contents($configFile, $nginxConf);
        umask($umask);

        $configCopy = "/etc/nginx/sites-available/{$projectName}";
        $configLink = "/etc/nginx/sites-enabled/{$projectName}";

        $env->exec("sudo cp {$configFile} {$configCopy}");
        if (!file_exists($configLink)) {
            $env->exec("sudo ln -s {$configCopy} {$configLink}");
        }

        @mkdir($projectRoot . DIRECTORY_SEPARATOR . 'log', Env::DIR_PERMISSIONS);

        $env->exec('sudo service nginx restart');
    }

    public function execCustomCode() : void
    {
        $env = $this->_env;
        if (!empty($env->config['exec']['common']) && $env->config['exec']['common'] instanceof \Closure) {
            $env->config['exec']['common']($env);
        }
        $envName = $env->getName();
        if (!empty($env->config['exec'][$envName]) && $env->config['exec'][$envName] instanceof \Closure) {
            $env->config['exec'][$envName]($env);
        }
    }
}