<?php
return [
    'remotes' => [
        'REMOTE_NAME' => [
            'user' => 'USER_NAME',
            'domain' => 'DOMAIN_OR_IP',
            'port' => 22,
            'project_root' => 'PROJECT_ROOT_DIR',
        ],
    ],
    'hosts' => [
        'dev' => [
            'welcome.dev' => '', //example: exchange.dev
            'staff.dev' => '', //example: staff.exchange.dev
            'welcome.test' => '', //example: exchange.test
            'staff.test' => '' //example: staff.exchange.test
        ],
        'stage' => [
            'welcome.stage' => '', //example: exchange.stage
            'staff.stage' => '', //example: staff.exchange.stage
        ],
        'prod' => [
            'welcome.prod' => '',
            'staff.prod' => '',
        ],
    ],
    'webroots' => [
        'dev' => [
            'welcome' => '', //example: http://exchange.dev
            'staff' => '', //example: http://staff.exchange.dev
        ],
        'stage' => [
            'welcome' => '', //example: http://exchange.stage
            'staff' => '', //example: http://staff.exchange.stage
        ],
        'prod' => [
            'welcome' => '',
            'staff' => '',
        ]
    ],
    //code executed when using command 'main' with 'custom' option or command 'custom'
    'exec' => [
        'common' => function(\fs\init\Env $env) {
        },
        'dev' => function(\fs\init\Env $env) {
        },
        'stage' => function(\fs\init\Env $env) {
        },
        'prod' => function(\fs\init\Env $env) {
        },
    ],
    'remote_commands' => [
        'before' => [
            'common' => [], //example: git pull
            'dev' => [],
            'stage' => [],
            'prod' => [],
        ],
        'after' => [
            'common' => [],
            'dev' => [],
            'stage' => [],
            'prod' => [],
        ]
    ]
];