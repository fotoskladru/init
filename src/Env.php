<?php

namespace fs\init;

/**
 * Class Env
 *
 * @package fs\init
 */
class Env
{
    const ENVIRONMENTS = ['dev', 'stage', 'prod'];
    const DIR_PERMISSIONS = 0775;

    /**
     * @var array
     */
    public $config = [];

    /**
     * @var string
     */
    protected $_projectRoot;

    /**
     * @var string
     */
    protected $_envBranch;

    /**
     * @var string
     */
    protected $_fullName;

    /**
     * @var array
     */
    protected $_params = [];

    /**
     * @var string
     */
    protected $_command;

    /**
     * Env constructor.
     *
     * @param string $projectRoot
     *
     * @throws \ErrorException
     */
    public function __construct(string $projectRoot)
    {
        $this->_projectRoot = $projectRoot;
        $configPath = $this->_projectRoot . DIRECTORY_SEPARATOR . 'deploy' . DIRECTORY_SEPARATOR . 'config.php';

        if (file_exists($configPath)) {
            $this->config = require($this->_projectRoot . DIRECTORY_SEPARATOR . 'deploy' . DIRECTORY_SEPARATOR . 'config.php');
        } else {
            $this->config = $this->createEmptyConfig($configPath);
        }

        $rawParams = [];
        if (isset($_SERVER['argv'])) {
            $rawParams = $_SERVER['argv'];
            array_shift($rawParams);
        }

        if (!empty($rawParams[0])) {
            $this->_command = array_shift($rawParams);
            if (!in_array($this->_command, Command::getCommandsList())) {
                $help = 'init (' . implode('|', Command::getCommandsList()) . ') [--param=value]';
                throw new \ErrorException('Unknown command, try it: ' . PHP_EOL . $help);
            }
        } else {
            $help = 'init (' . implode('|', Command::getCommandsList()) . ') [--param=value]';
            throw new \ErrorException('Command required, try it:' . PHP_EOL . $help);
        }

        foreach ($rawParams as $param) {
            if (preg_match('/^--(\w+)(=(.*))?$/', $param, $matches)) {
                $name = $matches[1];
                $this->_params[$name] = isset($matches[3]) ? $matches[3] : true;
            } else {
                $this->_params[] = $param;
            }
        }

        if (!Command::isValidCommandFormat($this->_command, $this->_params)) {
            $help = "init {$this->_command} " . Command::getCommandHelp($this->_command);
            throw new \ErrorException('Invalid command format, try it:' . PHP_EOL . $help);
        }

        $this->_envBranch = $this->isDevelopment() || $this->isStage() ? 'development' : ($this->isProduction() ? 'production' : $this->getParam('env'));
        $this->_fullName = $this->isDevelopment() ? 'development' : ($this->isProduction() ? 'production' : $this->getParam('env'));
    }

    /**
     * @return string
     */
    public function getCommandName() : string
    {
        return $this->_command;
    }

    /**
     * @param string $name
     * @param string|null $defaultValue
     *
     * @return string|null
     */
    public function getParam(string $name, ?string $defaultValue = null) : ?string
    {
        return isset($this->_params[$name]) ? $this->_params[$name] : $defaultValue;
    }

    /**
     * @param string $name
     * @param string|null $defaultValue
     *
     * @return bool
     */
    public function getBoolParam(string $name, ?string $defaultValue = null) : bool
    {
        return '1' === $this->getParam($name, $defaultValue);
    }

    /**
     * @return string
     */
    public function getProjectRoot() : string
    {
        return $this->_projectRoot;
    }

    /**
     * @return string
     */
    public function getEnvBranch() : string
    {
        return $this->_envBranch;
    }

    /**
     * @return string
     */
    public function getCurrentBranch() : string
    {
        exec('git branch|grep "*"', $output);
        $branch = trim($output[0], '* ');

        return $branch;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->getParam('env');
    }

    /**
     * @return string
     */
    public function getFullName() : string
    {
        return $this->_fullName;
    }

    /**
     * @return bool
     */
    public function isDevelopment() : bool
    {
        return 'dev' == $this->getParam('env');
    }

    /**
     * @return bool
     */
    public function isStage() : bool
    {
        return 'stage' == $this->getParam('env');
    }

    /**
     * @return bool
     */
    public function isProduction() : bool
    {
        return 'prod' == $this->getParam('env');
    }

    /**
     * @param string $command
     * @param bool $printOutput
     * @param string|null $output
     */
    public function exec(string $command, bool $printOutput = false, ?string &$output = null) : void
    {
        echo $command, PHP_EOL;
        exec($command, $output);
        if ($printOutput) {
            echo implode(PHP_EOL, $output), PHP_EOL;
        }
    }

    /**
     * @param string $file
     *
     * @return array
     */
    protected function createEmptyConfig(string $file) : array
    {
        umask(0);
        @mkdir(dirname($file), static::DIR_PERMISSIONS, true);
        $source = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'config.php';
        copy($source, $file);
        $config = require $file;

        return $config;
    }
}