<?php

namespace fs\init;

/**
 * Class Command
 *
 * @package fs\init
 */
class Command
{

    /**
     * @param string $cmd
     * @param Env $env
     */
    public static function run(string $cmd, Env $env) : void
    {
        if (in_array($cmd, ['create', 'main', 'nginx', 'custom'], true)) {
            $init = new Init($env);
            switch ($cmd) {
                case 'create':
                    $init->createSkeleton();
                    break;
                case 'main':
                    $init->mainRun();
                    break;
                case 'nginx':
                    $init->setHosts();
                    $init->setNginx();
                    break;
                case 'custom':
                    $init->execCustomCode();
                    break;
            }
        } elseif (in_array($cmd, ['map', 'config'], true)) {
            $conf = new Config($env);
            switch ($cmd) {
                case 'map':
                    $conf->createMap();
                    break;
                case 'config':
                    $conf->createMap();
                    $conf->createConfig();
                    break;
            }
        }
    }

    /**
     * @return array
     */
    public static function getCommands() : array
    {
        $env = ['required' => true, 'options' => Env::ENVIRONMENTS];

        return [
            'create' => ['env' => $env],
            'main'   => [
                'env'    => $env,
                'perm'   => ['required' => false, 'options' => ['0', '1'], 'default' => '1'],
                'local'  => ['required' => false, 'options' => ['0', '1'], 'default' => '1'],
                'nginx'  => ['required' => false, 'options' => ['0', '1'], 'default' => '0'],
                'custom' => ['required' => false, 'options' => ['0', '1'], 'default' => '0'],
            ],
            'nginx'  => ['env' => $env],
            'custom' => ['env' => $env],
            'map'    => ['env' => $env],
            'config' => [
                'env'      => $env,
                'remote'   => ['required' => false],
                'priority' => ['required' => false, 'options' => Config::PRIORITIES, 'default' => 'local'],
            ],
        ];
    }

    /**
     * @return array
     */
    public static function getCommandsList() : array
    {
        return array_keys(static::getCommands());
    }

    /**
     * @param string $cmd
     *
     * @return null|string
     */
    public static function getCommandHelp(string $cmd) : ?string
    {
        $commands = static::getCommands();
        if (isset($commands[$cmd])) {
            $command = $commands[$cmd];
            $required = $optional = [];
            foreach ($command as $name => $params) {
                $options = !empty($params['options']) ? '(' . implode('|', $params['options']) . ')' : 'VALUE';
                $default = !empty($params['default']) ? " (default: {$params['default']})" : '';
                if ($params['required']) {
                    $required[] = "--{$name}={$options}{$default}";
                } else {
                    $optional[] = " [--{$name}={$options}{$default}]";
                }
            }

            return implode(' ', $required) . implode('', $optional);
        }

        return null;
    }

    /**
     * @param string $cmd
     * @param array $params
     *
     * @return bool
     */
    public static function isValidCommandFormat(string $cmd, array $params) : bool
    {
        $commands = static::getCommands();
        if (isset($commands[$cmd])) {
            $command = $commands[$cmd];
            foreach ($command as $name => $param) {
                if (!isset($params[$name])) {
                    if ($param['required'] == true) {
                        return false;
                    } else {
                        continue;
                    }
                } elseif (isset($param['options']) && !in_array($params[$name], $param['options'])) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $cmd
     * @param string $param
     *
     * @return null|string
     */
    public static function getDefault(string $cmd, string $param) : ?string
    {
        $commands = static::getCommands();
        if (isset($commands[$cmd]) && isset($commands[$cmd][$param]) && isset($commands[$cmd][$param]['default'])) {
            return $commands[$cmd][$param]['default'];
        }

        return null;
    }
}